{{ config(schema='stg')}}

SELECT guid AS player_id,
       brand as franchise_name,
       inactive_days,
       churn_segment, 
       churn_segment_previous,
       evaluation_day
FROM
{{ source('techflex_segment', 'churn_segment_history') }} a
