{{ config(schema='stg')}}




SELECT
  trim(body.id) as business_unit_id,
  upper(trim(body.name))  as business_unit_name,
  trim(body.type) as business_unit_type,
  trim(body.parentId) as parent_id,
  trim(body.hostname) as host_name,
 trim(body.initialPlayerStatus) as initial_player_status,
 CAST( body.minPlayerAge AS STRING ) as min_player_age,
 trim(body.baseCurrency) as base_currency,
 CAST( body.restricted AS BOOL) restricted,
 trim(body.sharingStrategy) AS sharing_strategy,
 CAST(body.updated AS TIMESTAMP) as date_updated

from 	{{ source('raw', 'business_units') }}

UNION ALL
SELECT 
 '0' as business_unit_id, 
 'N/A' as business_unit_name, 
 'N/A' as business_unit_type, 
 '0' as parent_id,
 'N/A' as host_name,
 'N/A' as initial_player_status,
 'N/A' as min_player_age,
 'N/A' as base_currency,
 NULL as restricted,
 'N/A' as sharing_strategy,
 CAST('1900-01-01 00:00:00' AS TIMESTAMP) as date_updated,

