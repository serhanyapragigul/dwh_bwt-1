{{ config(schema='stg')}}

SELECT
	TRIM(`type`) as type,
    TRIM(body.id) as game_id,
	TRIM(body.gameId) as internal_game_id,
	TRIM(body.subClassificationName) as sub_classification_name, 
	TRIM(body.slug) as slug, 
	TRIM(body.providerGameId) as provider_game_id, 
	TRIM(body.providerId) as provider_id,
	TRIM(body.gameConfiguration.title) as title,
	TRIM(body.mainClassificationId) as main_classification_id,
	TRIM(body.subProviderID) as sub_provider_id, 
	CAST(body.published as TIMESTAMP) as date_published, 
	CAST(body.updated as TIMESTAMP) date_updated
FROM {{ source('raw', 'games') }} gm
