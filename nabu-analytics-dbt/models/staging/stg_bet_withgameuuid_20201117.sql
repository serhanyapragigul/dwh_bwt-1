{{ config(schema='stg')}}


SELECT
      TRIM(betId) AS bet_id
    , TRIM(game_uuid) AS game_uuid
    , TRIM(game_providerGameId) AS game_providerGameId
    , TRIM(providerName) AS provider_name
    , TRIM(providerId) AS provider_id
    , TRIM(providerType) AS provider_type
    , TRIM(subClassificationName) AS sub_classification_name
    , TRIM(bets_providerGameId) AS bets_provider_game_id
    , TRIM(bets_providerType) AS bets_provider_type
    , TRIM(bets_internalGameId) AS bets_internal_game_id
    , TRIM(bets_subGameId) AS bets_sub_game_id
    , bets_created AS bets_created
    , TRIM(bets_kafkaData.topic) AS bets_kafka_topic
    , bets_kafkaData.partition AS bets_kafka_partition
    , bets_kafkaData.offset AS bets_kafka_offset
    , bets_kafkaData.insertTime AS bets_kafka_insert_time
    , TRIM(bets_kafkaData.rowUniqueId) AS bets_kafka_row_unique_id
    , TRIM(kafkaData.topic) AS kafka_topic
    , kafkaData.partition AS kafka_partition
    , kafkaData.offset AS kafka_offset
    , kafkaData.insertTime AS kafka_insert_time
    , TRIM(kafkaData.rowUniqueId) AS kafka_row_unique_id

FROM {{ source('raw', 't_raw_bets_withgameuuid_20201117') }}

        