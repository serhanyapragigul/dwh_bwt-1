    {{ config(schema='stg')}}

SELECT
    player_id,
    franchise_name,
    business_unit_id,
    a.product_segment,
    sub_segment
FROM
{{ source('techflex_segment', 'product_segment') }} a