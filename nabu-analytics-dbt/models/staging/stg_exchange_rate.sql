  {{ config(schema='stg')}}

    SELECT  
        TRIM(r.type) as type, 
        TRIM(r.body.baseCurrency) as base_currency, 
        CAST(r.body.updated AS TIMESTAMP)  as start_date,
        TRIM(rates.key) as currency,
        cast(rates.value as numeric)  as exchange_rate,
        kafkadata.* 
          
    FROM {{ source('raw', 'exchange_rates') }} r
			left join unnest (body.rates) as rates