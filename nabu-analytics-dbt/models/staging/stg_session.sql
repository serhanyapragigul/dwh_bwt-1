{{ config(schema='stg')}}

SELECT
	FARM_FINGERPRINT(CONCAT(kafkaData.topic,':',kafkaData.PARTITION,':',kafkaData.offset)) AS session_key,
	type as type,
	TRIM(body.id) as session_id,
	TRIM(body.userId) as player_id,
	TRIM(body.businessUnitId) as business_unit_id,
	CAST(body.created as TIMESTAMP) as date_created,
	CAST(body.expiration as TIMESTAMP) as date_expiration,
	TRIM(body.locationDetails.country) as country,
	TRIM(body.locationDetails.city) as city,
	TRIM(body.locationDetails.ip) as ip,
	TRIM(body.deviceDetails.userAgent) as user_agent,
	TRIM(body.deviceDetails.browser) as browser,
	TRIM(body.deviceDetails.browserVersion) as browser_version,
	TRIM(body.deviceDetails.platform) as platform,	
	TRIM(body.deviceDetails.platformVersion) as platform_version,
	TRIM(body.deviceDetails.deviceType) as device_type,
	_PARTITIONTIME as partition_time,
	kafkaData.insertTime AS kafka_insert_time
FROM {{ source('raw', 'sessions') }}
	