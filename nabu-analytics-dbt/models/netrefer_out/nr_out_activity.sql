{{	config(
		schema='netrefer_out', 
		materialized='view',
        tags=['daily']
	)
}}
/*
    Allocation matrix is used to provide correct allocation for payment amount when a payment goes through multiple captured--> not captured statuses across multiple days
    We fix the last is_captured state for each day. Then, if there are multiple days in a row with the same state, we combine them into 1 row with the first date_updated
    Further in code, we multiply payment amount by this payment allocation value, meaning that if a captured payment became not captured on the day after capturing or later,
    We subtract it, i.e., show with the same negative value
*/
WITH allocation_matrix AS (
    SELECT  pmt_agg.payment_id,
            pmt_agg.activity_date,
            CASE 
                WHEN pmt_agg.is_captured THEN 1
                WHEN NOT pmt_agg.is_captured AND pmt_agg.prev_status IS NULL THEN 0
                WHEN NOT pmt_agg.is_captured THEN -1
            END AS payment_allocation
    FROM (
        SELECT  daily_payment.payment_id,
                daily_payment.activity_date,
                daily_payment.is_captured,
                LAG(daily_payment.is_captured) OVER (PARTITION BY daily_payment.payment_id ORDER BY daily_payment.activity_date) AS prev_status
        FROM (
            SELECT  DISTINCT pmt.payment_id,
                    CAST(pmt.date_updated AS DATE) AS activity_date,
                    LAST_VALUE(CASE WHEN pmt.payment_status = 'CAPTURED' THEN TRUE ELSE FALSE END) OVER (PARTITION BY pmt.payment_id, CAST(pmt.date_updated AS DATE) ORDER BY pmt.date_updated ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS is_captured
            FROM (
                SELECT  p.payment_id, 
                        p.payment_status, 
                        p.date_updated,
                        FIRST_VALUE(CASE WHEN p.payment_status = 'CAPTURED' THEN p.date_updated END IGNORE NULLS) OVER (PARTITION BY p.payment_id ORDER BY p.date_updated ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_captured
                FROM {{ ref('dwh_payment') }} AS p
                WHERE TRUE 
                QUALIFY date_captured <= p.date_updated
            ) AS pmt
        ) AS daily_payment
        WHERE TRUE
        QUALIFY prev_status != daily_payment.is_captured OR prev_status IS NULL
    ) AS pmt_agg
)
SELECT  rt.player_netrefer_id AS CustomerID,
        COALESCE(rt.test_account, FALSE) AS is_testaccount,
        FORMAT_DATETIME('%Y-%m-%d %H:%M:%S', COALESCE(am.activity_date, CAST(rt.transaction_date AS DATE))) AS ActivityDate,
        CAST(COALESCE(ap.product_id, 0) AS int64) AS ProductID,
        CAST(COALESCE(abr.brand_id, 1) AS int64) AS BrandID,
        CAST(COALESCE(ROUND(SUM(rt.gamewin_eur), 2), 0) AS NUMERIC) AS GrossRevenue,
        CAST(COALESCE(ROUND(SUM(rt.total_bonus_cost_eur), 2), 0) AS NUMERIC) AS Bonus,
        CAST(COALESCE(ROUND(SUM(rt.adjustment_amt_eur), 2), 0) AS NUMERIC) AS Adjustments,
        CAST(COALESCE(ROUND(SUM(rt.turnover_eur), 2), 0) AS NUMERIC) AS Turnover,
        CAST(COALESCE(ROUND(SUM(
        CASE
            WHEN rt.record_type = 'PAYMENT' AND rt.transaction_type = 'DEPOSIT' THEN rt.deposit_initiated_amt_eur * am.payment_allocation
/*
	Adding fake NDC records to fetch NDC correctly because NetRefer seems to skip zero Deposits records while calculating NDCs
	Such records may appear due to further cancellation (we subtract them above). But we still need to count them as NDC according to our logic
	https://nabu.atlassian.net/wiki/spaces/BI/pages/456294401/BI+Glossary#Newly-Depositing-Customer-(NDC)
    since all values on Netrefer side are rounded to 2 decimal digits, adding amount with more precision (4 digits in this case) doesn't affect total figures for reporting much
    according to average values of NDCs per month (~3000 NDCs overall, meaning 100 per day), all NDCs could affect daily Deposits amount by 0.01 EUR at maximum which is affordable
    but at the end of the day, it guarantees having a record with a value > 0 rather than ignoring refused/refunded NDCs
*/
            WHEN rt.record_type = 'NDC' THEN 0.0001
            ELSE 0
        END 
        ), 2), 0) AS NUMERIC) AS Deposits,
        CAST(COALESCE(ROUND(SUM(
        CASE
            WHEN rt.record_type = 'PAYMENT' AND rt.transaction_type = 'WITHDRAWAL' THEN rt.withdrawal_initiated_amt_eur * am.payment_allocation
            ELSE 0
        END 
        ), 2), 0) AS NUMERIC) AS Withdrawals,
        CAST(0 AS NUMERIC) AS Payouts,
        CAST(
            COALESCE(COUNT(DISTINCT 
                CASE 
                    WHEN rt.record_type = 'BONUS' AND rt.given_bonus_amt_eur > 0 THEN rt.transaction_id --calculate only number of claimed bonuses from f_bonus rather than all records from f_bonus_bet
                    WHEN rt.record_type NOT IN ('PAYMENT', 'BONUS') THEN rt.transaction_id 
                    ELSE NULL 
                END), 0) +
            COALESCE(SUM(am.payment_allocation), 0)
        AS int64) AS Transactions,
        CAST(COALESCE(ap.sub_product_id, 0) AS int64) AS SubProductID,
		CAST(COALESCE(ab.bonus_type_id, 0) AS int64) AS BonusTypeID,
		CAST(COALESCE(adm.deposit_method_id, 0) AS int64) AS DepositMethodID,
        CAST(0 AS int64) AS Points,
        COALESCE(am.activity_date, CAST(rt.transaction_date AS DATE)) AS data_date
FROM {{ ref('f_reporting_transactions') }} AS rt
    LEFT JOIN {{ ref('dwh_affiliate_brand') }} AS abr
	ON rt.business_unit_id = abr.brand_uuid
        LEFT JOIN {{ ref('dwh_affiliate_product') }} AS ap 
		ON rt.provider_type = ap.provider_type
			AND rt.main_classification_name = ap.main_classification_name
            LEFT JOIN allocation_matrix AS am 
            ON am.payment_id = rt.transaction_id
                LEFT JOIN {{ ref('dwh_affiliate_bonus') }} AS ab 
			    ON ab.bonus_type = rt.transaction_type
                    AND rt.record_type = 'BONUS'
                    LEFT JOIN {{ ref('dwh_affiliate_depositmethod') }} AS adm
					ON rt.payment_method_type = adm.deposit_method_name
WHERE ((rt.record_type = 'BET' AND rt.final_status IN ('WON', 'CLOSED', 'CLOSED_WITH_LATE_WINS'))
        OR (rt.record_type = 'ADJUSTMENT' AND rt.final_status = 'APPROVED')
            OR (rt.record_type = 'BONUS' AND rt.is_final_bonus_status)
                OR rt.record_type IN ('PAYMENT', 'NDC'))
                    AND abr.brand_id IS NOT NULL 
GROUP BY    CustomerID,
            is_testaccount,
            ActivityDate,
            ProductID,
            BrandID,
            SubProductID,
            BonusTypeID,
            DepositMethodID,
            data_date