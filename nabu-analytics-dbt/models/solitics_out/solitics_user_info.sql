{{ config(schema='solitics_out')}}

WITH data_prep AS
(
  SELECT  player_id,
          FORMAT_TIMESTAMP('%F %H:%M:%E3S %Z', COALESCE(registration_date, timestamp_millis(0))) AS registration_date,
          COALESCE(business_unit_id, 'N/A') AS business_unit_id,
          COALESCE(value_segment, 'N/A') AS value_segment,
          COALESCE(value_seg_30d_highest, 'N/A') AS value_seg_30d_highest,
          COALESCE(value_seg_ever_highest, 'N/A') AS value_seg_ever_highest,
          COALESCE(product_segment, 'N/A') AS product_segment,
          COALESCE(sub_segment, 'N/A') AS sub_segment,
          CAST(is_bonus_abuser AS STRING) AS is_bonus_abuser,
          COALESCE(kyc_status, 'N/A') AS kyc_status,
          FORMAT_TIMESTAMP('%F %H:%M:%E3S %Z', COALESCE(kyc_verified_date,  timestamp_millis(0))) AS kyc_verified_date,
          COALESCE(days_since_last_active, 0) AS days_since_last_active,
          COALESCE(affiliate_id, 'N/A') AS affiliate_id,
          COALESCE(channel, 'N/A') AS channel,
          COALESCE(first_deposit_method, 'N/A') AS first_deposit_method,
          COALESCE(bettor_type, 'N/A') AS bettor_type, 
          COALESCE(ratio_desktop, 0) AS ratio_desktop, 
          COALESCE(ratio_mobile, 0 ) AS ratio_mobile,
          COALESCE(average_bet_stake_eur_sb, 0) AS average_bet_stake_eur_sb,
          COALESCE(biggest_bet_amount_eur_sb, 0 ) AS biggest_bet_amount_eur_sb,
          COALESCE(most_played_team, 'N/A') AS most_played_team,  
          COALESCE(most_played_sport, 'N/A') AS most_played_sport,
          COALESCE(most_played_league, 'N/A') AS most_played_league,
          COALESCE(most_played_market, 'N/A') AS most_played_market,
          COALESCE(bonus_ratio_7d, 0.0) AS bonus_ratio_7d,
          COALESCE(bonus_ratio_14d, 0.0) AS bonus_ratio_14d,
          COALESCE(bonus_ratio_30d, 0.0) AS bonus_ratio_30d,
          COALESCE(bonus_ratio_lifetime, 0.0) AS bonus_ratio_lifetime,
          COALESCE(payment_first_method_type, 'N/A') AS payment_first_method_type,
          COALESCE(payment_first_method_score, 0.0) AS payment_first_method_score,
          COALESCE(payment_second_method_type, 'N/A') AS payment_second_method_type,
          COALESCE(payment_second_method_score, 0.0) AS payment_second_method_score,
          COALESCE(payment_recommendation, 'N/A') AS payment_recommendation,
          COALESCE(payment_first_generic_method_offer, 'N/A') AS payment_first_generic_method_offer,
          COALESCE(payment_second_generic_method_offer, 'N/A') AS payment_second_generic_method_offer,
          COALESCE(payment_third_generic_method_offer, 'N/A') AS payment_third_generic_method_offer,
          COALESCE(churn_last_evaluation_day, '1900-01-01') AS churn_last_evaluation_day,
          COALESCE(churn_inactive_days, -1) AS churn_inactive_days, 
          COALESCE(churn_segment, 'N/A') AS churn_segment,	
          COALESCE(churn_segment_previous, 'N/A') AS churn_segment_previous,
          COALESCE(sb_realm_player_color, 'N/A') AS sb_realm_player_color
     FROM {{ ref('pb_player_report') }}
)
SELECT player_id,
       '{' || '\n'
        || '"memberId": "' || player_id || '",' || '\n'
        || '"registrationDate": "' || registration_date || '",' || '\n'
        || '"customFields": ' || '\n' 
        || '    {' || '\n'
        || '    "bi_businessUnitId": "' || business_unit_id || '",\n'
        || '    "bi_valueSegment": "' || value_segment || '",\n'
        || '    "bi_valueSeg30dHighest": "' || value_seg_30d_highest || '",\n'
        || '    "bi_valueSegEverHighest": "' || value_seg_ever_highest || '",\n'
        || '    "bi_productSegment": "' || product_segment || '",\n'
        || '    "bi_subSegment": "' || sub_segment || '",\n'
        || '    "bi_isBonusAbuser": "' || is_bonus_abuser || '",\n'
        || '    "bi_kycStatus": "' || kyc_status || '",\n'
        || '    "bi_kycVerifiedDate": "' || kyc_verified_date || '",\n'
        || '    "bi_daysSinceLastActive": ' || days_since_last_active || ',\n'
        || '    "bi_affiliateId": "' || affiliate_id || '",\n'
        || '    "bi_channel": "' || channel || '",\n'
        || '    "bi_firstDepositMethod": "' || first_deposit_method || '",\n'
        || '    "bi_bettorType": "' || bettor_type || '",\n'
        || '    "bi_ratioDesktop": ' || ratio_desktop || ',\n'
        || '    "bi_ratioMobile": ' || ratio_mobile || ',\n'
        || '    "bi_averageBetStakeEurSb": ' || average_bet_stake_eur_sb || ',\n'
        || '    "bi_biggestBetAmountEurSb": ' || biggest_bet_amount_eur_sb || ',\n'
        || '    "bi_mostPlayedTeam": "' || most_played_team || '",\n'
        || '    "bi_mostPlayedSport": "' || most_played_sport || '",\n'
        || '    "bi_mostPlayedLeague": "' || most_played_league || '",\n'
        || '    "bi_mostPlayedMarket": "' || most_played_market || '",\n'
        || '    "bi_bonusRatio7d": ' || bonus_ratio_7d || ',\n'
        || '    "bi_bonusRatio14d": ' || bonus_ratio_14d || ',\n'
        || '    "bi_bonusRatio30d": ' || bonus_ratio_30d || ',\n'
        || '    "bi_bonusRatioLifetime": ' || bonus_ratio_lifetime || ',\n'
        || '    "bi_paymentFirstMethodType": "' || payment_first_method_type || '",\n'
        || '    "bi_paymentFirstMethodScore": ' || payment_first_method_score || ',\n'
        || '    "bi_paymentSecondMethodType": "' || payment_second_method_type || '",\n'
        || '    "bi_paymentSecondMethodScore": ' || payment_second_method_score || ',\n'
        || '    "bi_paymentRecommendation": "' || payment_recommendation || '",\n'
        || '    "bi_paymentFirstGenericMethodOffer": "' || payment_first_generic_method_offer || '",\n'
        || '    "bi_paymentSecondGenericMethodOffer": "' || payment_second_generic_method_offer || '",\n'
        || '    "bi_paymentThirdGenericMethodOffer": "' || payment_third_generic_method_offer || '",\n'
        || '    "bi_churnLastEvaluationDay": "' || churn_last_evaluation_day || '",\n'
        || '    "bi_churnInactiveDays": ' || churn_inactive_days || ',\n'
        || '    "bi_churnSegment": "' || churn_segment || '",\n'
        || '    "bi_churnSegmentPrevious": "' || churn_segment_previous || '",\n'
        || '    "bi_sbRealmPlayerColor": "' || sb_realm_player_color || '"\n'
        || '    }' || '\n'
        || '}'   AS json_val
  FROM data_prep
