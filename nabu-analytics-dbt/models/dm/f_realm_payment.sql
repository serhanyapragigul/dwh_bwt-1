{{ 
    config(
        schema='dm',
        materialized='incremental',
        unique_key='public_payment_id',
        clustered_by = ['status_date', 'public_payment_id']
        )
}}

SELECT DISTINCT pmt.public_payment_id,
                LAST_VALUE(pmt.operator) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS opearator,
                LAST_VALUE(pmt.status_date) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status_date,
                LAST_VALUE(pmt.create_date) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS create_date,
                FIRST_VALUE(
	                CASE
	                    WHEN pmt.status = 'CAPTURED' THEN pmt.status_date
	                    ELSE NULL
	                END IGNORE NULLS
	                ) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS captured_date,
                LAST_VALUE(pmt.brand_name) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS brand_name,
                LAST_VALUE(pmt.currency_code) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS currency_code,
                LAST_VALUE(pmt.amount) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS amount,
                LAST_VALUE(pmt.amount_eur) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS amount_eur,
                LAST_VALUE(pmt.fee) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fee,
                LAST_VALUE(pmt.fee_eur) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fee_eur,
                LAST_VALUE(pmt.payment_type) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_type,
                LAST_VALUE(pmt.provider_name) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_name,
                LAST_VALUE(pmt.method_name) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS method_name,
                LAST_VALUE(pmt.channel_name) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS channel_name,
                LAST_VALUE(pmt.bin_series) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS bin_series,
                LAST_VALUE(pmt.country_code) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS country_code,
                LAST_VALUE(pmt.customer_guid) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS customer_guid,
                LAST_VALUE(pmt.status) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status,
                LAST_VALUE(pmt.reason) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS reason,
                LAST_VALUE(pmt.browser) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS browser,
                LAST_VALUE(pmt.os) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS os,
                LAST_VALUE(pmt.is_private_lobby) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS is_private_lobby,
                LAST_VALUE(pmt.is_first_deposit) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS is_first_deposit,
                LAST_VALUE(pmt.file_name) OVER (PARTITION BY pmt.public_payment_id ORDER BY pmt.status_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS file_name
    FROM {{ source('dwh', 'dwh_realm_payment') }} pmt
    {% if is_incremental() %}
        WHERE pmt.status_date > (SELECT MAX(status_date) FROM {{ this }})
    {% endif -%}