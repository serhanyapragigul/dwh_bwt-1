{{
    config(
        schema='dm',
        materialized='incremental',
        unique_key='unique_id',
        cluster_by=['player_id', 'business_unit_id', 'coupon_id', 'bet_id'],
		post_hook="{% if is_incremental() %} DROP TABLE IF EXISTS  
					{% if target.name == 'prod' %}
					dm
					{% else %}
					{{target.schema}}
					{% endif %}.tmp_coupon_ids_to_update {% endif %}"		
        )
}}
--too many coupons can be changed during an increment, so that the length of a resulted query exceeds 1024Kb limit
--thus, it's not reliable to use arrays for coupon_id to be updated, let's use a temp table
{% if is_incremental() %}
	{%- set table_name = 'dm.tmp_coupon_ids_to_update' if target.name == 'prod' else target.schema + '.tmp_coupon_ids_to_update' -%}
	{%- call statement('need_update', True) -%}
		CREATE OR REPLACE TABLE {{ table_name}} AS
		SELECT	coupon_id
		FROM {{ ref('dwh_sb_coupon') }}
		WHERE date_placed > (SELECT MAX(date_coupon_placed) FROM {{ this }})
		UNION DISTINCT
		SELECT	coupon_id
		FROM {{ ref('dwh_sb_couponStateChanged') }}
		WHERE COALESCE(date_changed, date_time_stamp) > (SELECT MAX(date_coupon_changed) FROM {{ this }})
		UNION DISTINCT
		SELECT	coupon_id
		FROM {{ ref('dwh_sb_betStatusChanged_betSelection') }}
		WHERE COALESCE(date_changed, date_time_stamp) > (SELECT LEAST(MAX(date_bet_changed), MAX(date_betselection_changed)) FROM {{ this }})
		UNION DISTINCT
		SELECT	coupon_id
		FROM {{ ref('dwh_sb_betCashedOut') }}
		WHERE COALESCE(bet_cashedout_processed_date,bet_cashedout_date_time_stamp) > (SELECT MAX(cashout_date) FROM {{ this }});
		SELECT	CASE
					WHEN COUNT(*) = 0 THEN FALSE
					ELSE TRUE
				END
		FROM {{ table_name }}
	{%- endcall %}
	{%- set need_update = load_result('need_update')['data'][0][0] -%}
{% endif %}
SELECT	FARM_FINGERPRINT(CONCAT(main.coupon_id, main.bet_id, main.bet_selection_id)) AS unique_id,
		main.coupon_id,
		main.bet_id,
		main.bet_selection_id,
		main.coupon_initial_status,
		main.date_coupon_placed,
		main.coupon_final_status,
		main.date_coupon_changed,
		main.bet_final_status,
		main.date_bet_changed,
		main.betselection_initial_status,
		main.date_betselection_changed,
		main.betselection_final_status,
		main.brand_id,
		main.player_id,
		main.business_unit_id,
		buh.franchise_name,
		main.partner_id,
		main.bet_type,
		main.bonus_type,
		main.review_status,
		main.bet_status,
		main.result_status,
		main.closed_reason,
		main.odds_value,
		main.odds_boosted_value,
		main.combinedodds_value,
		main.combinedodds_boosted_value,
		main.channel,
		main.bet_game_phase,
		main.betstake_user_currency_code,
		main.betstake_user_currency_amount,
		main.betstake_user_currency_amount * er.exchange_rate AS betstake_eur,
		main.totalRedeemedStake_user_currency_code,
		main.totalRedeemedStake_user_currency_amount,
		main.totalRedeemedStake_user_currency_amount * er.exchange_rate AS redeemed_eur,
		main.market_selection_id,
		main.market_selection_name,
		main.event_id,
		main.event_name,
		main.event_start_date,
		main.game_phase,
		main.sport_id,
		main.sport_name,
		main.competition_id,
		main.competition_name,
		main.market_id,
		main.market_name,
		main.participant,
		main.sub_participant,
		main.line,
		main.void_reason,
		main.betselections_odds_value,
		main.betselections_odds_boosted_value,
		cashed_out.cashout_state,
		main.changed_cashout_state,
		cashed_out.cashout_date,
		cashed_out.base_currency_cashed_out * er_cashout.exchange_rate AS cashed_out_EUR
FROM (
	SELECT	DISTINCT betselect.coupon_id,
			betselect.bet_id,
			betselect.bet_selection_id,
			betselect.coupon_status AS coupon_initial_status,
			betselect.date_placed AS date_coupon_placed,
			a.coupon_status AS coupon_final_status,
			a.date_changed AS date_coupon_changed,
			c.result_status AS bet_final_status,
			c.date_changed AS date_bet_changed,
			betselect.betselections_bet_status AS betselection_initial_status,
			b.date_changed AS date_betselection_changed,
			b.settled_status AS betselection_final_status,
			betselect.brand_id,
			betselect.player_id,
			plr.business_unit_id,
			betselect.partner_id,
			betselect.bet_type,
			betselect.bonus_type,
			betselect.review_status,
			betselect.bet_status,
			betselect.result_status,
			betselect.closed_reason,
			betselect.odds_value,
			betselect.odds_boosted_value,
			betselect.combinedodds_value,
			betselect.combinedodds_boosted_value,
			betselect.channel,
			betselect.bet_game_phase,
			betselect.betstake_user_currency_code,
			betselect.betstake_user_currency_amount,
			betselect.totalRedeemedStake_user_currency_code,
			betselect.totalRedeemedStake_user_currency_amount,
			betselect.market_selection_id,
			betselect.market_selection_name,
			betselect.event_id,
			betselect.event_name,
			sb_event.event_start_date,
			betselect.game_phase,
			betselect.sport_id,
			betselect.sport_name,
			betselect.competition_id,
			betselect.competition_name,
			betselect.market_id,
			betselect.market_name,
			CASE
				WHEN betselect.market_selection_name = '1' THEN TRIM(REGEXP_EXTRACT(betselect.event_name, r'(.*) - '))
				WHEN betselect.market_selection_name = '2' THEN TRIM(REGEXP_EXTRACT(betselect.event_name, r'\ - (.*)'))
				ELSE betselect.participant 
			END AS participant,
			betselect.sub_participant,
			betselect.line,
			betselect.void_reason,
			betselect.betselections_odds_value,
			betselect.betselections_odds_boosted_value,
			cashoutStateChanged.changed_cashout_state
	FROM {{ ref('dwh_sb_coupon') }} AS betselect
		LEFT JOIN {{ ref('d_player') }} AS plr
		ON betselect.player_id = plr.player_id
			LEFT JOIN (
				SELECT	DISTINCT coupon_id, 
						coupon_status, 
						FIRST_VALUE(COALESCE(date_changed, date_time_stamp) IGNORE NULLS) OVER (PARTITION BY coupon_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_changed 
				FROM (
					SELECT	DISTINCT coupon_id, 
							coupon_status, 
							date_changed , 
							date_time_stamp,
							LAST_VALUE(coupon_status IGNORE NULLS) OVER (PARTITION BY coupon_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status,
					FROM {{ ref('dwh_sb_couponStateChanged') }}
					{% if is_incremental() and need_update %}
						WHERE coupon_id IN (SELECT coupon_id FROM {{ table_name }})
					{% elif is_incremental() and not need_update %}
						WHERE {{ need_update }}
					{% endif %}
					)
				WHERE coupon_status = status
				) AS a
			ON betselect.coupon_id = a.coupon_id
				LEFT JOIN (
					SELECT	DISTINCT bet_id, 
							result_status, 
							FIRST_VALUE(COALESCE(date_changed, date_time_stamp) IGNORE NULLS) OVER (PARTITION BY bet_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_changed 
					FROM (
						SELECT	DISTINCT bet_id, 
						result_status, 
						date_changed,
						date_time_stamp,
						LAST_VALUE(result_status IGNORE NULLS) OVER (PARTITION BY bet_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status
						FROM {{ ref('dwh_sb_betStatusChanged_betSelection') }}
						{% if is_incremental() and need_update %}
							WHERE coupon_id IN (SELECT coupon_id FROM {{ table_name }})
						{% elif is_incremental() and not need_update %}
							WHERE {{ need_update }}
						{% endif %}
						)
						WHERE result_status = status
						) AS c
				ON betselect.bet_id = c.bet_id
					LEFT JOIN (
						SELECT	DISTINCT bet_selection_id, 
								settled_status, 
								FIRST_VALUE(COALESCE(date_changed, date_time_stamp) IGNORE NULLS) OVER (PARTITION BY bet_selection_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_changed 
						FROM (
							SELECT	DISTINCT bet_selection_id, 
									settled_status, 
									date_changed, 
									date_time_stamp,
									LAST_VALUE(settled_status IGNORE NULLS) OVER (PARTITION BY bet_selection_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status
							FROM {{ ref('dwh_sb_betStatusChanged_betSelection') }}
							{% if is_incremental() and need_update %}
								WHERE coupon_id IN (SELECT coupon_id FROM {{ table_name }})
							{% elif is_incremental() and not need_update %}
								WHERE {{ need_update }}
							{% endif %}
							)
						WHERE settled_status = status) AS b
					ON betselect.bet_selection_id = b.bet_selection_id
						LEFT JOIN (
							SELECT	DISTINCT bet_selection_id, 
									bet_closed_reason, 
									FIRST_VALUE(COALESCE(date_changed, date_time_stamp) IGNORE NULLS) OVER (PARTITION BY bet_selection_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS date_changed 
							FROM (
								SELECT	DISTINCT bet_selection_id, 
										bet_closed_reason, 
										date_changed, 
										date_time_stamp,
										LAST_VALUE(bet_closed_reason IGNORE NULLS) OVER (PARTITION BY bet_selection_id ORDER BY COALESCE(date_changed, date_time_stamp) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS status
								FROM {{ ref('dwh_sb_betStatusChanged_betSelection') }}
								{% if is_incremental() and need_update %}
									WHERE coupon_id IN (SELECT coupon_id FROM {{ table_name }})
								{% elif is_incremental() and not need_update %}
									WHERE {{ need_update }}
								{% endif %}
								)
								WHERE bet_closed_reason = status
								) AS d
						ON betselect.bet_selection_id = d.bet_selection_id
							LEFT JOIN {{ ref('dwh_sb_cashoutStateChanged') }} AS cashoutStateChanged
							ON betselect.bet_id = cashoutStateChanged.bet_id
								LEFT JOIN {{ ref('dwh_sb_event') }} AS sb_event
								ON betselect.event_id = sb_event.event_id
	{% if is_incremental() and need_update %}
		WHERE betselect.coupon_id IN (SELECT coupon_id FROM {{ table_name }})
	{% elif is_incremental() and not need_update %}
		WHERE {{ need_update }}
	{% endif %}					
	) AS main
	LEFT JOIN (
		SELECT	DISTINCT cashout_state,
				coupon_id AS cashed_out_coupon_id,
				bet_id AS cashed_out_bet_id,
				currencyCode AS cashout_currency_code,
				MAX(COALESCE(bet_cashedout_processed_date,bet_cashedout_date_time_stamp)) OVER (PARTITION BY coupon_id, bet_id) AS cashout_date,
				SUM(amount) OVER(PARTITION BY coupon_id, bet_id) AS base_currency_cashed_out
		FROM {{ref('dwh_sb_betCashedOut')}}
		{% if is_incremental() and need_update %}
			WHERE coupon_id IN (SELECT coupon_id FROM {{ table_name }})
		{% elif is_incremental() and not need_update %}
			WHERE {{ need_update }}
		{% endif %}
		) AS cashed_out
	ON main.bet_id = cashed_out.cashed_out_bet_id
	LEFT JOIN {{ref('dwh_exchange_rate')}} AS er 
	ON main.betstake_user_currency_code = er.base_currency 
		AND main.date_coupon_placed >= er.start_date 
			AND main.date_coupon_placed < er.end_date 
				AND er.Currency = 'EUR'
		LEFT JOIN {{ref('dwh_exchange_rate')}} AS er_cashout 
		ON cashed_out.cashout_currency_code = er_cashout.base_currency 
			AND cashed_out.cashout_date >= er_cashout.start_date 
				AND cashed_out.cashout_date < er_cashout.end_date 
					AND er_cashout.Currency = 'EUR'
			LEFT JOIN {{ ref('d_business_unit_hierarchy') }} buh 
			ON main.business_unit_id = buh.franchise_id 
				AND buh.most_recent
