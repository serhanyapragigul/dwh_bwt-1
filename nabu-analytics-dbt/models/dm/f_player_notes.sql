{{ 
    config(
        schema='dm', 
        materialized='table'
    )
}}
SELECT  DISTINCT pn.player_notes_id, 
        LAST_VALUE(pn.player_id IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_id,
        LAST_VALUE(pn.business_unit_id IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS business_unit_id,
        buh.franchise_name, 
        LAST_VALUE(pn.note IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS note,
        LAST_VALUE(pn.pinned IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS pinned,
        LAST_VALUE(pn.player_notes_date IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_notes_date, 
        LAST_VALUE(pn.player_notes_updated_by IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_notes_updated_by,
        LAST_VALUE(pn.player_notes_updated_name IGNORE NULLS) OVER (PARTITION BY player_notes_id ORDER BY player_notes_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_notes_update_name
        /*
        pl.player_Status as current_Player_Status,
        pl.Player_Status_Date  as current_Player_Status_Date,
        pl.test_account
        */
FROM {{ ref('dwh_player_notes') }} AS pn
    LEFT JOIN {{ ref('d_business_unit_hierarchy') }} AS buh 
    ON pn.business_unit_id = buh.franchise_id
        AND buh.most_recent
WHERE   pn.type <> 'DELETED'
/*some weird unverified stuff further, I leave it commented just in case someone remembers why we should have it
LEFT JOIN
--verify what this join is suppost to do
(
SELECT a.player_id, 
b.player_Status, a.player_Status_Date, test_account from
    ( select player_id, player_Status , MIN(date_updated) as player_Status_Date from {{ ref('dwh_player') }} group by player_id, player_Status ) a
    join
    ( select player_id, player_status , date_updated, test_account, ROW_NUMBER() OVER (PARTITION BY player_id ORDER by date_updated DESC) AS row_no from 
            {{  ref('dwh_player') }} ) b on a.player_id = b.player_id and a.player_status = b.player_status AND b.row_no = 1
) pl
ON pn.player_id = pl.player_id 
*/



