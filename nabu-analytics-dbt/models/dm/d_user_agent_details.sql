{{
	config(
		schema='dm',
		materialized='incremental',
		unique_key = 'user_agent'
		)
}}

SELECT
      user_agent
    , user_agent_type
    , user_agent_brand
    , user_agent_name
    , user_agent_url
    , user_agent_os_name
    , user_agent_os_code
    , user_agent_os_url
    , user_agent_os_family
    , user_agent_os_family_code
    , user_agent_os_family_vendor
    , user_agent_os_icon
    , user_agent_os_icon_large
    , user_agent_device_is_mobile_device
    , user_agent_device_type
    , user_agent_device_brand
    , user_agent_device_brand_code
    , user_agent_device_brand_url
    , user_agent_device_name
    , user_agent_browser_name
    , user_agent_browser_version
    , user_agent_browser_version_major
    , user_agent_browser_engine
    , user_agent_crawler_is_crawler
    , user_agent_crawler_category
    , user_agent_crawler_last_seen
FROM
    {{ ref('dwh_session_user_agent') }}
