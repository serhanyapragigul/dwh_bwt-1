{{ config(schema='dm')}}

SELECT sub.player_key,
	   sub.player_id,
       -- COALESCE logic: use dwh_player, if data not available use dwh_player_registration
	   COALESCE(pr.business_unit_id, sub.businessunit_id) AS business_unit_id, -- There is more than 1 insnaces of business_unit_id for a player in dwh so use the player_registration, maybe brak loose in future when we know how new federated brands will work
	   COALESCE(sub.first_name, pr.first_name) AS first_name,
	   COALESCE(sub.last_name, pr.last_name) AS last_name,
	   sub.display_name,
	   COALESCE(sub.gender, pr.gender) AS gender,
	   COALESCE(sub.address, pr.address) AS address,
	   COALESCE(sub.country_code, pr.country) AS country_code,
	   cntr.name AS country_name,
	   COALESCE(sub.city, pr.city) AS city,
	   COALESCE(sub.postal_code, pr.postal_code) AS postal_code,
	   COALESCE(sub.currency_code, pr.currency) AS currency_code,
	   COALESCE(sub.latest_phone_nr, sub.phone_number) AS phone_number,
	   COALESCE(sub.email, pr.email) AS email,
	   COALESCE(sub.terms, pr.terms) AS terms,
	   COALESCE(sub.privacy_policy, pr.privacy_policy) AS privacy_policy,
	   sub.kyc_status,
	   sub.player_status,
	   --sub.eligibilities, --remove as it is confusing and not directly needed, JIRA BT-41.
	   COALESCE(sub.birth_date, pr.birth_date) AS birth_date,
	   COALESCE( pr.registered_date, sub.registration_date) AS registration_date,-- There is more than 1 instnces of business_unit_id for a player in dwh so use the player_registration, maybe brak loose in future when we know how new federated brands will work
	   CASE WHEN sub.player_status != 'BLOCKED_BY_RMT' THEN COALESCE( pr.registered_date, sub.registration_date) ELSE NULL END AS nrc_date,
	   CASE WHEN sub.player_status != 'BLOCKED_BY_RMT' THEN 1 ELSE 0 END AS count_as_nrc,
	   affiliate.affiliate_tag as affiliate_tag,
	   SPLIT(affiliate.affiliate_tag,'_')[SAFE_OFFSET(0)] as affiliate_id,
	   sub.date_updated,
	   sub.updated_by,
	   sub.updated_by_username,
	   sub.test_account,
	   sub.numeric_id,
	   pr.user_ip,
	   pr.language,
	   pr.session_id,
	   buh.merchant_name,
	   buh.brand_name,
	   buh.franchise_name,
	   er.reference,
	   pr.player_registration_id,
	   segment.product_segment,
	   segment.sub_segment,
	   sub.kyc_verified_date,
	   first_used_device_type, 
	   COALESCE(bal.fraud_cnt, 0) > 0  AS is_bonus_abuser,
	   COALESCE(ARRAY_LENGTH(bal.all_statuses), 0) AS realm_account_cnt,
	   COALESCE(bal.fraud_cnt, 0) AS realm_fraud_account_cnt,
	   COALESCE(bal.sb_realm_player_color, 'N/A') AS sb_realm_player_color, 
	   COALESCE(bal.vip_cnt > 0, FALSE) AS is_realm_valuable_player,
	   CASE WHEN COALESCE(ARRAY_LENGTH(bal.all_statuses), 0) = 0 THEN NULL ELSE ARRAY_TO_STRING(bal.all_statuses, ', ') END AS all_statuses,
	   COALESCE(segm.value_segment, 'N/A') AS value_segment,
	   COALESCE(segm.value_seg_30d_highest, 'N/A') AS value_seg_30d_highest,
	   COALESCE(hval.highest_val, 'N/A') AS value_seg_ever_highest
FROM
(
SELECT * 
  FROM 
    (
	SELECT p.player_key,
		   p.player_id,
		   p.businessunit_id,
		   p.first_name,
		   p.last_name,
		   p.display_name,
		   p.gender,
		   p.address,
		   p.country_code,
		   p.city,
		   p.postal_code,
		   p.currency_code,
		   p.phone_number,
		   p.email,
		   p.terms,
		   p.privacy_policy,
		   p.kyc_status,
		   p.player_status,
		   --p.eligibilities,
		   p.birth_date,
		   p.registration_date,
		   p.date_updated,
		   p.updated_by,
		   p.updated_by_username,
		   p.test_account,
		   p.manual_affiliate_tag,
		   p.numeric_id,
		   FIRST_VALUE(CASE WHEN kyc_status = 'VERIFIED' THEN date_updated ELSE NULL END IGNORE NULLS) OVER (PARTITION BY player_id ORDER BY date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS kyc_verified_date,
		   LAST_VALUE(phone_number IGNORE NULLS) OVER(PARTITION BY player_id ORDER BY date_updated) latest_phone_nr,
		   ROW_NUMBER() OVER (PARTITION BY p.player_id ORDER by p.date_updated desc) AS row_no,
		   CASE WHEN email LIKE '%+%@%'
		   		  THEN REPLACE(CONCAT(SUBSTRING(email,0, INSTR(email,'+')-1),	SUBSTRING(email, INSTR(email,'@'))),'.','')
		   		ELSE REPLACE(email,'.','')
		   END AS clean_email
	FROM {{ ref('dwh_player') }} p
	) 
-- Use the latest information available in dwh_player
WHERE row_no = 1
) sub

LEFT JOIN {{ ref('dwh_player_registration') }} pr
	ON sub.player_id = pr.player_id

LEFT JOIN
	(
	SELECT platform_name, 
	       operator_name, 
		   merchant_name, 
		   brand_name, 
		   franchise_id, 
		   franchise_name, 
		   host_name, 
		   start_date, 
		   end_date,
		   most_recent
    FROM  {{ ref('d_business_unit_hierarchy') }}
		WHERE most_recent is TRUE
	) buh
	ON pr.business_unit_id = buh.franchise_id 

--JiRA bt-155 --For affiliate tag, tag affiliate_tag if CREATED, if CREATED is NULL take last manual_affiliate_tag, if manual_affiliate_tag is NULL take 658512; google 
LEFT JOIN (
		SELECT DISTINCT player_id,
			FIRST_VALUE(affiliate_tag) OVER( PARTITION BY player_id ORDER BY date_updated ASC) affiliate_tag
		FROM (
			SELECT player_id,
					COALESCE(	CASE WHEN `type` = 'CREATED' THEN affiliate_tag ELSE NULL END, 
								CASE 
									WHEN LAST_VALUE(manual_affiliate_tag IGNORE NULLS) OVER(PARTITION BY player_id ORDER BY date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) = "" THEN NULL 
									ELSE last_value(manual_affiliate_tag IGNORE NULLS) OVER(PARTITION BY player_id ORDER BY date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) 
								END,
							'658512') AS affiliate_tag,
					date_updated
			FROM {{ ref('dwh_player') }}
			)
) AS affiliate
	ON sub.player_id = affiliate.player_id

LEFT JOIN
	(
		SELECT DISTINCT player_id, 
			reference
		FROM {{ ref('dwh_external_references') }}
	) er
	ON sub.player_id = er.player_id

LEFT JOIN
	(
		SELECT DISTINCT player_id,
			business_unit_id,
			product_segment,
			sub_segment
		FROM {{ ref('dwh_product_segment') }} 
	) segment
	ON  sub.player_id = segment.player_id

LEFT JOIN 
	(
		SELECT	bal_all.clean_email,
				ARRAY_AGG(bal_all.last_status ORDER BY bal_all.last_status) AS all_statuses ,
				SUM(CAST(is_fraud_status AS INT64)) AS fraud_cnt,
				SUM(CAST(LOWER(realm_segment) LIKE '%vip%' AS INT64))  AS vip_cnt,
				MAX(current_sb_classification) AS sb_realm_player_color,
				COUNT(current_sb_classification) AS player_cnt
		FROM
		(
			SELECT *, 
			LAST_VALUE(current_sb_classification) OVER (PARTITION BY clean_email ORDER BY rank_id DESC, realm_player_id ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING  ) fin_current_sb_classification
			FROM (
			-- betsson abuser email list
			SELECT	DISTINCT rl.clean_email, 
					rl.realm_player_id,
					LAST_VALUE(rl.status_name) OVER (PARTITION BY rl.clean_email, rl.realm_player_id ORDER BY rl.retrieval_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS last_status,
					LAST_VALUE(rl.is_fraud_status) OVER (PARTITION BY rl.clean_email, rl.realm_player_id ORDER BY rl.retrieval_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS is_fraud_status,
					LAST_VALUE(rl.realm_segment) OVER (PARTITION BY rl.clean_email, rl.realm_player_id ORDER BY rl.retrieval_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS realm_segment,
					LAST_VALUE(rl.current_sb_classification) OVER (PARTITION BY rl.clean_email, rl.realm_player_id ORDER BY rl.retrieval_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS current_sb_classification,
					LAST_VALUE(sc.rank_id) OVER (PARTITION BY rl.clean_email, rl.realm_player_id ORDER BY rl.retrieval_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rank_id
			FROM {{ ref('dwh_realm_customer_status') }} rl
			LEFT JOIN {{ ref('dwh_player_sb_color_ranking')}} sc
			  ON rl.current_sb_classification = sc.color_name
			WHERE is_valid_email	
			) bal_win
		) AS bal_all 
		GROUP BY bal_all.clean_email
	) AS bal
		ON sub.clean_email = bal.clean_email 

LEFT JOIN {{ref('dwh_value_segment')}} segm 
	ON sub.player_id = segm.player_id
   		AND buh.franchise_name = segm.franchise_name

LEFT JOIN (
	SELECT DISTINCT 
		   player_id, 
		   business_unit_id,
		   FIRST_VALUE(device_type IGNORE NULLS ) OVER (PARTITION BY player_id, business_unit_id ORDER BY date_created) AS first_used_device_type
	FROM (
		SELECT DISTINCT
		       LAST_VALUE(player_id IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY date_created ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) player_id,
		       LAST_VALUE(business_unit_id IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY date_created ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) business_unit_id,
			   LAST_VALUE(device_type IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY date_created ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) device_type,
			   LAST_VALUE(date_created IGNORE NULLS) OVER (PARTITION BY session_id ORDER BY date_created ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) date_created
		FROM {{ref('dwh_session')}}

		)
	WHERE player_id IS NOT NULL
	) sessions 
	ON sub.player_id = sessions.player_id
		AND  COALESCE(pr.business_unit_id, sub.businessunit_id) = sessions.business_unit_id

LEFT JOIN 
	(
	SELECT DISTINCT  
	       s.player_id, 
		   s.franchise_name, 
		   FIRST_VALUE(s.value_segment) OVER (
    			PARTITION BY s.player_id 
				ORDER BY r.rank_num, s.effective_from
				ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING 


			) AS highest_val
 	FROM {{ref('dwh_value_segment_history')}} s 
    LEFT JOIN {{ref('dwh_value_segment_rank_history')}} r 
    	 on s.value_segment = r.value_segment
    		AND s.effective_from >= r.effective_from
    			AND s.effective_from <= r.effective_to
	) hval
	ON  sub.player_id = hval.player_id
	AND buh.franchise_name = hval.franchise_name
LEFT JOIN {{ ref('dwh_country_codes_snapshot') }} AS cntr 
	ON COALESCE(sub.country_code, pr.country) = cntr.code 
	AND cntr.dbt_valid_to IS NULL
