{{ config(schema='dm')}}

SELECT  DISTINCT payment_accounts_id,
        FIRST_VALUE (player_id IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as player_id,
        FIRST_VALUE (external_account_id IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as external_account_id, 
        FIRST_VALUE (method_type IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as method_type, 
        LAST_VALUE (kyc_status IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as kyc_status,  
        LAST_VALUE (account_status IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as account_status,  
        LAST_VALUE (multi_usage IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as multi_usage,  
        FIRST_VALUE (date_updated IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as date_created,  
        LAST_VALUE (date_updated IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as date_updated,
        LAST_VALUE (country IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as country,
        LAST_VALUE (crypto_currency IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as crypto_currency,
        LAST_VALUE (destination_tag IGNORE NULLS ) OVER (PARTITION BY payment_accounts_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as destination_tag
    FROM {{ ref('dwh_payment_account') }}

