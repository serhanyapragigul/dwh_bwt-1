{{ 
    config(
        schema='dm',
        materialized='table'
        )
}}

SELECT DISTINCT bl.bin,
                LAST_VALUE(bl.issuer) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS issuer,
                LAST_VALUE(bl.network) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS network,
                LAST_VALUE(bl.type) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS type,
                LAST_VALUE(bl.category) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS category,
                LAST_VALUE(bl.country_code) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS country_code,
                LAST_VALUE(bl.file_name) OVER (PARTITION BY bl.bin ORDER BY bl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS file_name
    FROM {{ source('dwh', 'dwh_realm_bin_list') }} bl