{{ config(schema='dm', materialized='view')}}
---d_game

SELECT
    gm.game_id, 
	internal_game_id, 
	sub_classification_name, 
	slug, 
	provider_game_id, 
	title, 
	--date_published, 
	date_updated,
	main_classification_name,
	main_classification_id,
	provider_name,
	provider_id,
	gm.sub_provider_id,
	gsp.sub_provider_name
FROM {{ ref('dwh_game') }} as gm


--Select the game_id which is most recently updated 
JOIN
(select game_id, max(date_updated) as upd
from {{ ref('dwh_game') }}
group by game_id) as sub

ON gm.game_id = sub.game_id and gm.date_updated = sub.upd

left JOIN

(
	select
		DISTINCT
		LAST_VALUE (sub_provider_id IGNORE NULLS ) OVER (PARTITION BY sub_provider_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sub_provider_id,
		LAST_VALUE (sub_provider_name IGNORE NULLS ) OVER (PARTITION BY sub_provider_id ORDER by date_updated asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as sub_provider_name,
from {{ref('dwh_game_sub_providers')}}
) gsp
on gm.sub_provider_id = gsp.sub_provider_id



 