{{ 
    config(
        schema='dm',
        materialized='table'
        )
}}

SELECT DISTINCT pl.id,
                LAST_VALUE(pl.supplier) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS supplier,
                LAST_VALUE(pl.method_class) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS method_class,
                LAST_VALUE(pl.method_name) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS method_name,
                LAST_VALUE(pl.provider_name) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_name,
                LAST_VALUE(pl.payment_type) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_type,
                LAST_VALUE(pl.file_name) OVER (PARTITION BY pl.id ORDER BY pl.file_name ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS file_name
    FROM {{ source('dwh', 'dwh_realm_provider_list') }} pl