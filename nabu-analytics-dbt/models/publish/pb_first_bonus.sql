{{
    config(
        schema='publish',
        materialized='table',
		tags=['daily'],
    )
}}

SELECT
        bonus.first_bonus_id,
        bonus.first_bonus_payment_id,
        bonus.first_bonus_type,
        bonus.first_bonus_amount_eur,
        payment.provider_name as first_bonus_provider_name
FROM(
SELECT 
		distinct
		player_id,
		business_unit_id,
		FIRST_VALUE (bonus_id) OVER (PARTITION BY player_id,business_unit_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_bonus_id,
		FIRST_VALUE (payment_id) OVER (PARTITION BY player_id,business_unit_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_bonus_payment_id,
		FIRST_VALUE (bonus_type) OVER (PARTITION BY player_id,business_unit_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_bonus_type,
		FIRST_VALUE (amount_eur) OVER (PARTITION BY player_id,business_unit_id ORDER by date_created asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_bonus_amount_eur,
from {{ ref('f_bonus') }})  as bonus

LEFT JOIN
(SELECT
		DISTINCT
		payment_id,
		provider_name

FROM {{ ref('f_payment') }}) as payment
on bonus.first_bonus_payment_id = payment.payment_id
