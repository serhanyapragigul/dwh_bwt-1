{{
    config(
        schema='publish',
        materialized='table',
		tags=['daily'],
        cluster_by = ['reporting_month']
    )
}}

SELECT
    FARM_FINGERPRINT(CONCAT(activity_history.reporting_month, activity_history.player_id, activity_history.business_unit_id)) AS player_retention_id,
	activity_history.reporting_month,
	activity_history.player_id,
	activity_history.business_unit_id,
	activity_history.active_current_month,
	activity_history.active_previous_month,
	activity_history.active_historically,
	CASE
		WHEN active_current_month
			AND NOT active_previous_month
			AND NOT active_historically THEN 'NAC'
		WHEN active_current_month
			AND active_previous_month THEN 'RAC'
		WHEN active_current_month
			AND NOT active_previous_month
			AND active_historically THEN 'REACT'
		WHEN NOT active_current_month
			AND active_previous_month THEN 'CHURN'
		WHEN NOT active_current_month
			AND NOT active_previous_month
			AND active_historically THEN 'INACTIVE'
	END AS retention_status
FROM
	(
	SELECT
		rm.reporting_month,
		fam.player_id,
		fam.business_unit_id,
		ap.activity_month IS NOT NULL AS active_current_month,
		LAG(ap.activity_month) OVER ( PARTITION BY fam.player_id,
		fam.business_unit_id
	ORDER BY
		rm.reporting_month ASC) IS NOT NULL AS active_previous_month,
		COALESCE( SUM(CASE WHEN ap.activity_month IS NOT NULL THEN 1 END) OVER ( PARTITION BY fam.player_id,
		fam.business_unit_id
	ORDER BY
		rm.reporting_month ASC ROWS BETWEEN UNBOUNDED PRECEDING AND 2 PRECEDING) > 0,
		FALSE) AS active_historically
	FROM
		(
		SELECT
			reporting_month
		FROM
			UNNEST( GENERATE_DATE_ARRAY( (
			SELECT
				MIN(DATE(TIMESTAMP_TRUNC(activity_date, MONTH)))
			FROM
				{{ ref('f_active_player') }}),
			DATE_TRUNC(CURRENT_DATE,
			MONTH),
			INTERVAL 1 MONTH) ) AS reporting_month ) AS rm
	INNER JOIN (
		SELECT
			player_id,
			business_unit_id,
			MIN(DATE(TIMESTAMP_TRUNC(activity_date, MONTH))) AS first_active_month,
		FROM
			{{ ref('f_active_player') }}
		GROUP BY
			player_id,
			business_unit_id ) AS fam ON
		rm.reporting_month >= fam.first_active_month
	LEFT JOIN (
		SELECT
			DISTINCT player_id,
			business_unit_id,
			DATE(TIMESTAMP_TRUNC(activity_date,
			MONTH)) AS activity_month
		FROM
			{{ ref('f_active_player') }} ) AS ap ON
		fam.player_id = ap.player_id
		AND fam.business_unit_id = ap.business_unit_id
		AND rm.reporting_month = ap.activity_month
	) AS activity_history