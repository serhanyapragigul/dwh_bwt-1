{{
    config(
        schema='publish',
        materialized='table',
        cluster_by = ['value_type', 'reporting_date', 'reporting_period', 'metric_name']
        )
}}

WITH dates AS (
SELECT  reporting_date, 
        reporting_period,
        FORMAT_DATE(
            CASE reporting_period
                WHEN 'YTD' THEN '%Y'
                WHEN 'QTD' THEN '%YQ%Q'
                WHEN 'MTD' THEN '%YM%m'
            END,
            reporting_date
        ) AS reporting_period_code,
        comparison_period,
        CASE reporting_period 
            WHEN 'MTD' THEN DATE_SUB(DATE_TRUNC(reporting_date, MONTH), INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) MONTH)
            WHEN 'QTD' THEN DATE_SUB(DATE_TRUNC(reporting_date, QUARTER), INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) QUARTER)
            WHEN 'YTD' THEN DATE_SUB(DATE_TRUNC(reporting_date, YEAR), INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) YEAR)
        END AS period_start,
        CASE reporting_period
            WHEN 'MTD' THEN DATE_SUB(reporting_date, INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) MONTH)
            WHEN 'QTD' THEN DATE_SUB(reporting_date, INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) QUARTER)
            WHEN 'YTD' THEN DATE_SUB(reporting_date, INTERVAL (CASE comparison_period WHEN 'PREVIOUS' THEN 1 ELSE 0 END) YEAR)
        END AS period_end
FROM UNNEST(GENERATE_DATE_ARRAY('2021-01-01', CURRENT_DATE(), INTERVAL 1 DAY)) AS reporting_date
    CROSS JOIN UNNEST(['MTD', 'QTD', 'YTD']) AS reporting_period
        CROSS JOIN UNNEST(['CURRENT', 'PREVIOUS']) AS comparison_period
),
main AS (
    SELECT  apb_formatted.reporting_date,
            apb_formatted.reporting_period,
            apb_formatted.reporting_period_code,
            apb_formatted.period_start,
            apb_formatted.period_end,
            apb_formatted.previous_period_start,
            apb_formatted.previous_period_end,
            apb_formatted.franchise_name,
            apb_formatted.product,
            apb_formatted.metric_name,
            apb_formatted.value_type,
            apb_formatted.current_value,
            apb_formatted.previous_value
    FROM (
        SELECT  apb_corrected.reporting_date,
                apb_corrected.reporting_period,
                apb_corrected.reporting_period_code,
                apb_corrected.period_start,
                apb_corrected.period_end,
                apb_corrected.previous_period_start,
                apb_corrected.previous_period_end,
                apb_corrected.franchise_name,
                apb_corrected.product,
                apb_corrected.metric_name,
                apb_corrected.actual_value,
                apb_corrected.previous_value,
                apb_corrected.budget_value +
                CASE 
                    WHEN FIRST_VALUE(apb_corrected.real_budget_value) OVER (PARTITION BY apb_corrected.reporting_period_code, apb_corrected.franchise_name, apb_corrected.product, apb_corrected.metric_name ORDER BY apb_corrected.real_budget_value ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) IS NULL
                        AND LAST_VALUE(apb_corrected.real_budget_value) OVER (PARTITION BY apb_corrected.reporting_period_code, apb_corrected.franchise_name, apb_corrected.product, apb_corrected.metric_name ORDER BY apb_corrected.real_budget_value ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) IS NOT NULL
                            AND apb_corrected.real_budget_value IS NOT NULL
                    THEN LAST_VALUE(apb_corrected.actual_value) OVER (PARTITION BY apb_corrected.reporting_period_code, apb_corrected.franchise_name, apb_corrected.product, apb_corrected.metric_name ORDER BY apb_corrected.real_budget_value DESC, apb_corrected.reporting_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    ELSE 0
                END AS budget_value,
                apb_corrected.previous_budget_value
        FROM (
            SELECT  apb_corrected_first.reporting_date,
                    apb_corrected_first.reporting_period,
                    apb_corrected_first.reporting_period_code,
                    apb_corrected_first.period_start,
                    apb_corrected_first.period_end,
                    apb_corrected_first.previous_period_start,
                    apb_corrected_first.previous_period_end,
                    apb_corrected_first.franchise_name,
                    apb_corrected_first.product,
                    apb_corrected_first.metric_name,
                    apb_corrected_first.actual_value,
                    apb_corrected_first.previous_value,
                    apb_corrected_first.budget_value,
                    apb_corrected_first.real_budget_value,
                    CAST(
                            CASE bv_prev.unit
                                WHEN '%' THEN AVG(bv_prev.budget_amount)
                                ELSE SUM(bv_prev.daily_budget_amount)
                            END AS NUMERIC
                        ) AS previous_budget_value    
            FROM (
                SELECT  apb.reporting_date,
                        apb.reporting_period,
                        apb.reporting_period_code,
                        apb.period_start,
                        apb.period_end,
                        COALESCE(apb.previous_period_start, 
                            CASE apb.reporting_period 
                                WHEN 'MTD' THEN DATE_SUB(DATE_TRUNC(apb.reporting_date, MONTH), INTERVAL 1 MONTH)
                                WHEN 'QTD' THEN DATE_SUB(DATE_TRUNC(apb.reporting_date, QUARTER), INTERVAL 1 QUARTER)
                                WHEN 'YTD' THEN DATE_SUB(DATE_TRUNC(apb.reporting_date, YEAR), INTERVAL 1 YEAR)
                            END) AS previous_period_start,
                        COALESCE(apb.previous_period_end,
                            CASE apb.reporting_period
                                WHEN 'MTD' THEN DATE_SUB(apb.reporting_date, INTERVAL 1 MONTH)
                                WHEN 'QTD' THEN DATE_SUB(apb.reporting_date, INTERVAL 1 QUARTER)
                                WHEN 'YTD' THEN DATE_SUB(apb.reporting_date, INTERVAL 1 YEAR)
                            END) AS previous_period_end,
                        apb.franchise_name,
                        apb.product,
                        apb.metric_name,
                        apb.actual_value,
                        apb.previous_value,
                        CAST(COALESCE(SUM(bv.daily_budget_amount), apb.actual_value) AS NUMERIC) AS budget_value, 
                        SUM(bv.daily_budget_amount) AS real_budget_value
                FROM (
                    SELECT  actual_prev.reporting_date,
                            actual_prev.reporting_period,
                            actual_prev.reporting_period_code,
                            actual_prev.period_start_CURRENT AS period_start,
                            actual_prev.period_end_CURRENT AS period_end,
                            actual_prev.period_start_PREVIOUS AS previous_period_start,
                            actual_prev.period_end_PREVIOUS AS previous_period_end,
                            actual_prev.franchise_name,
                            actual_prev.product,
                            actual_prev.metric_name,
                            actual_prev.actual_CURRENT AS actual_value,
                            actual_prev.actual_PREVIOUS AS previous_value
                    FROM (
                        SELECT  unpivot_actuals.reporting_date,
                                unpivot_actuals.reporting_period,
                                unpivot_actuals.reporting_period_code,
                                unpivot_actuals.comparison_period,
                                unpivot_actuals.period_start,
                                unpivot_actuals.period_end,
                                unpivot_actuals.franchise_name, 
                                unpivot_actuals.product, 
                                REPLACE(unpivot_actuals.metric_name, '_', ' ') AS metric_name,
                                unpivot_actuals.actual_value
                        FROM (
                            SELECT  *
                            FROM(
                                SELECT  unioned_actuals.reporting_date,
                                        unioned_actuals.reporting_period,
                                        unioned_actuals.reporting_period_code,
                                        unioned_actuals.comparison_period,
                                        unioned_actuals.period_start,
                                        unioned_actuals.period_end,
                                        unioned_actuals.franchise_name, 
                                        unioned_actuals.product,
                                        CAST(unioned_actuals.Turnover AS NUMERIC) AS Turnover,
                                        CAST(unioned_actuals.Gamewin AS NUMERIC) AS Gamewin,
                                        CAST(unioned_actuals.Accounting_Revenue AS NUMERIC) AS Accounting_Revenue,
                                        CAST(unioned_actuals.Total_Bonus_Cost AS NUMERIC) AS Total_Bonus_Cost,
                                        CAST(unioned_actuals.Given_Bonus AS NUMERIC) AS Given_Bonus,
                                        CAST(unioned_actuals.Deposit_Captured_Amount AS NUMERIC) AS Deposit_Captured_Amount,
                                        CAST(unioned_actuals.Deposits_Captured AS NUMERIC) AS Deposits_Captured,
                                        CAST(unioned_actuals.Withdrawal_Captured_Amount AS NUMERIC) AS Withdrawal_Captured_Amount,
                                        CAST(unioned_actuals.Withdrawals_Captured AS NUMERIC) AS Withdrawals_Captured,
                                        CAST(unioned_actuals.NRC AS NUMERIC) AS NRC,
                                        CAST(unioned_actuals.NDC AS NUMERIC) AS NDC,
                                        CAST(unioned_actuals.Active_Customers AS NUMERIC) AS Active_Customers,
                                        CAST(unioned_actuals.Gross_Active_Customers AS NUMERIC) AS Gross_Active_Customers,
                                        CAST(unioned_actuals.Depositors AS NUMERIC) AS Depositors,
                                        CAST(unioned_actuals.Withdrawers AS NUMERIC) AS Withdrawers,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Deposit_Captured_Amount, unioned_actuals.Deposits_Captured) AS NUMERIC) AS Avg_Deposit_Amount,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Withdrawal_Captured_Amount, unioned_actuals.Withdrawals_Captured) AS NUMERIC) AS Avg_Withdrawal_Amount,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Deposit_Captured_Amount, unioned_actuals.Depositors) AS NUMERIC) AS Avg_Deposit_Amount_per_Depositor,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Withdrawal_Captured_Amount, unioned_actuals.Withdrawers) AS NUMERIC) AS Avg_Withdrawal_Amount_per_Depositor,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Deposits_Captured, unioned_actuals.Depositors) AS NUMERIC) AS Avg_Deposit_Count_per_Depositor,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Withdrawals_Captured, unioned_actuals.Withdrawers) AS NUMERIC) AS Avg_Withdrawal_Count_per_Depositor,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Turnover, unioned_actuals.Active_Customers) AS NUMERIC) AS ATPU,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Total_Bonus_Cost, unioned_actuals.Active_Customers) AS NUMERIC) AS ABCPU,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Gamewin, unioned_actuals.Active_Customers) AS NUMERIC) AS AGWPU,
                                        CAST(unioned_actuals.Cash_Reward AS NUMERIC) AS Cash_Reward,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Gamewin, unioned_actuals.Total_Bonus_Cost) AS NUMERIC) AS GW_TBC_Ratio,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Gamewin, unioned_actuals.Deposit_Captured_Amount) AS NUMERIC) AS GW_Dep_Ratio,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Turnover, unioned_actuals.Deposit_Captured_Amount) AS NUMERIC) AS Turnover_Dep_Ratio,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Total_Bonus_Cost, unioned_actuals.Deposit_Captured_Amount) AS NUMERIC) AS TBC_Dep_Ratio,
                                        CAST(unioned_actuals.Deposits_Initiated AS NUMERIC) AS Deposits_Initiated,
                                        CAST(unioned_actuals.Deposit_Initiated_Amount AS NUMERIC) AS Deposit_Initiated_Amount,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Deposits_Captured, unioned_actuals.Deposits_Initiated) AS NUMERIC) AS NDC_Acceptance_Rate,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Depositors, unioned_actuals.Deposit_Initiators) AS NUMERIC) AS User_Acceptance_Rate,
                                        CAST(SAFE_DIVIDE(unioned_actuals.Accounting_Revenue, unioned_actuals.Deposit_Captured_Amount) AS NUMERIC) AS Hold,
                                FROM ( 
                                    SELECT  d.reporting_date,
                                            d.reporting_period,
                                            d.reporting_period_code,
                                            d.comparison_period,
                                            d.period_start,
                                            d.period_end,
                                            mr.franchise_name, 
                                            mr.product,
                                            SUM(mr.turnover_eur) AS Turnover,
                                            SUM(mr.gamewin_eur) AS Gamewin,
                                            SUM(mr.accounting_revenue_eur) AS Accounting_Revenue,
                                            SUM(mr.total_bonus_cost_eur) AS Total_Bonus_Cost,
                                            SUM(mr.given_bonus_amt_eur) AS Given_Bonus,
                                            SUM(mr.deposit_captured_amt_eur) AS Deposit_Captured_Amount,
                                            SUM(mr.deposit_captured_cnt) AS Deposits_Captured,
                                            SUM(mr.withdrawal_captured_amt_eur) AS Withdrawal_Captured_Amount,
                                            SUM(mr.withdrawal_captured_cnt) AS Withdrawals_Captured,
                                            SUM(mr.nrc_cnt) AS NRC,
                                            SUM(mr.ndc_cnt) AS NDC,
                                            COUNT(DISTINCT CASE WHEN mr.active_player THEN player_id ELSE NULL END) AS Active_Customers,
                                            COUNT(DISTINCT mr.player_id) AS Gross_Active_Customers,
                                            COUNT(DISTINCT CASE WHEN mr.deposit_captured_cnt > 0 THEN mr.player_id ELSE NULL END) AS Depositors,
                                            COUNT(DISTINCT CASE WHEN mr.deposit_initiated_cnt > 0 THEN mr.player_id ELSE NULL END) AS Deposit_Initiators,
                                            COUNT(DISTINCT CASE WHEN mr.withdrawal_captured_cnt > 0 THEN mr.player_id ELSE NULL END) AS Withdrawers,
                                            SUM(mr.adjustment_bonus_eur) AS Cash_Reward,
                                            SUM(mr.deposit_initiated_cnt) AS Deposits_Initiated,
                                            SUM(mr.deposit_initiated_amt_eur) AS Deposit_Initiated_Amount
                                    FROM {{ ref('pb_management_report') }} AS mr 
                                        INNER JOIN dates AS d
                                            ON CAST(mr.reporting_date AS DATE) BETWEEN d.period_start AND d.period_end 
                                    GROUP BY    d.reporting_date,
                                                d.reporting_period,
                                                d.reporting_period_code,
                                                d.comparison_period,
                                                d.period_start,
                                                d.period_end,
                                                mr.franchise_name, 
                                                mr.product
                                    UNION ALL 
                                    SELECT  d.reporting_date,
                                            d.reporting_period,
                                            d.reporting_period_code,
                                            d.comparison_period,
                                            d.period_start,
                                            d.period_end,
                                            mr.franchise_name, 
                                            'ALL' AS product,
                                            SUM(mr.turnover_eur) AS Turnover,
                                            SUM(mr.gamewin_eur) AS Gamewin,
                                            SUM(mr.accounting_revenue_eur) AS Accounting_Revenue,
                                            SUM(mr.total_bonus_cost_eur) AS Total_Bonus_Cost,
                                            SUM(mr.given_bonus_amt_eur) AS Given_Bonus,
                                            SUM(mr.deposit_captured_amt_eur) AS Deposit_Captured_Amount,
                                            SUM(mr.deposit_captured_cnt) AS Deposits_Captured,
                                            SUM(mr.withdrawal_captured_amt_eur) AS Withdrawal_Captured_Amount,
                                            SUM(mr.withdrawal_captured_cnt) AS Withdrawals_Captured,
                                            CAST(SUM(mr.nrc_cnt) AS NUMERIC) AS NRC,
                                            CAST(SUM(mr.ndc_cnt) AS NUMERIC) AS NDC,
                                            CAST(COUNT(DISTINCT CASE WHEN mr.active_player THEN player_id ELSE NULL END) AS NUMERIC) AS Active_Customers,
                                            COUNT(DISTINCT mr.player_id) AS Gross_Active_Customers,
                                            COUNT(DISTINCT CASE WHEN mr.deposit_captured_cnt > 0 THEN mr.player_id ELSE NULL END) AS Depositors,
                                            COUNT(DISTINCT CASE WHEN mr.deposit_initiated_cnt > 0 THEN mr.player_id ELSE NULL END) AS Deposit_Initiators,
                                            COUNT(DISTINCT CASE WHEN mr.withdrawal_captured_cnt > 0 THEN mr.player_id ELSE NULL END) AS Withdrawers,
                                            SUM(mr.adjustment_bonus_eur) AS Cash_Reward,
                                            SUM(mr.deposit_initiated_cnt) AS Deposits_Initiated,
                                            SUM(mr.deposit_initiated_amt_eur) AS Deposit_Initiated_Amount
                                    FROM {{ ref('pb_management_report') }} AS mr 
                                        INNER JOIN dates AS d
                                            ON CAST(mr.reporting_date AS DATE) BETWEEN d.period_start AND d.period_end 
                                    GROUP BY    d.reporting_date,
                                                d.reporting_period,
                                                d.reporting_period_code,
                                                d.comparison_period,
                                                d.period_start,
                                                d.period_end,
                                                mr.franchise_name
                                    ) AS unioned_actuals
                            ) AS actual_values
                                UNPIVOT (actual_value FOR metric_name IN (
                                    Turnover,
                                    Gamewin,
                                    Accounting_Revenue,
                                    Total_Bonus_Cost,
                                    Given_Bonus,
                                    Deposit_Captured_Amount,
                                    Deposits_Captured,
                                    Withdrawal_Captured_Amount,
                                    Withdrawals_Captured,
                                    NRC,
                                    NDC,
                                    Active_Customers,
                                    Gross_Active_Customers,
                                    Depositors,
                                    Withdrawers,
                                    Avg_Deposit_Amount,
                                    Avg_Withdrawal_Amount,
                                    Avg_Deposit_Amount_per_Depositor,
                                    Avg_Withdrawal_Amount_per_Depositor,
                                    Avg_Deposit_Count_per_Depositor,
                                    Avg_Withdrawal_Count_per_Depositor,
                                    ATPU,
                                    ABCPU,
                                    AGWPU,
                                    Cash_Reward,
                                    GW_TBC_Ratio,
                                    GW_Dep_Ratio,
                                    Turnover_Dep_Ratio,
                                    TBC_Dep_Ratio,
                                    Deposits_Initiated,
                                    Deposit_Initiated_Amount,
                                    NDC_Acceptance_Rate,
                                    User_Acceptance_Rate,
                                    Hold
                                ))
                                ) AS unpivot_actuals
                            )
                            PIVOT(  SUM(actual_value) AS actual, 
                                    MAX(period_start) AS period_start, 
                                    MAX(period_end) AS period_end 
                                    FOR comparison_period IN ('CURRENT', 'PREVIOUS')
                    ) AS actual_prev
                ) AS apb --actual-previous-budget
                LEFT JOIN {{ ref('f_throne_budget_values') }} AS bv
                ON apb.franchise_name = bv.franchise
                    AND apb.product = bv.product
                        AND apb.metric_name = bv.metric_name
                            AND CAST(bv.reporting_date AS DATE) BETWEEN apb.period_start AND apb.period_end
                GROUP BY    apb.reporting_date,
                            apb.reporting_period,
                            apb.reporting_period_code,
                            apb.period_start,
                            apb.period_end,
                            previous_period_start,
                            previous_period_end,
                            apb.franchise_name,
                            apb.product,
                            apb.metric_name,
                            bv.unit,
                            apb.actual_value,
                            apb.previous_value
                ) AS apb_corrected_first --corrected running sum for missing budget values
                    LEFT JOIN {{ ref('f_throne_budget_values') }} AS bv_prev
                        ON apb_corrected_first.franchise_name = bv_prev.franchise
                            AND apb_corrected_first.product = bv_prev.product
                                AND apb_corrected_first.metric_name = bv_prev.metric_name
                                    AND CAST(bv_prev.reporting_date AS DATE) BETWEEN apb_corrected_first.previous_period_start AND apb_corrected_first.previous_period_end
                GROUP BY    apb_corrected_first.reporting_date,
                            apb_corrected_first.reporting_period,
                            apb_corrected_first.reporting_period_code,
                            apb_corrected_first.period_start,
                            apb_corrected_first.period_end,
                            apb_corrected_first.previous_period_start,
                            apb_corrected_first.previous_period_end,
                            apb_corrected_first.franchise_name,
                            apb_corrected_first.product,
                            apb_corrected_first.metric_name,
                            bv_prev.unit,
                            apb_corrected_first.actual_value,
                            apb_corrected_first.previous_value,
                            apb_corrected_first.budget_value,
                            apb_corrected_first.real_budget_value
                ) AS apb_corrected --corrected running sum for missing current budget values + previous budgets
            )
            UNPIVOT (
                (current_value, previous_value) 
                for value_type in (
                    (actual_value, previous_value) AS 'ACTUAL', 
                    (budget_value, previous_budget_value) AS 'BUDGET'
                )
        ) AS apb_formatted --final format unpivot for Tableau
    WHERE   apb_formatted.value_type = 'ACTUAL'
                OR (apb_formatted.value_type = 'BUDGET' AND apb_formatted.metric_name IN (SELECT DISTINCT metric_name FROM {{ ref('f_throne_budget_values') }}))
)

SELECT  main.reporting_date,
        main.reporting_period,
        main.reporting_period_code, 
        main.period_start,
        main.period_end,
        main.previous_period_start,
        main.previous_period_end,
        main.franchise_name,
        main.product,
        main.metric_name,
        main.value_type,
        main.current_value,
        main.previous_value
FROM main
UNION ALL
SELECT  calc_metrics.reporting_date,
        calc_metrics.reporting_period,
        calc_metrics.reporting_period_code, 
        calc_metrics.period_start,
        calc_metrics.period_end,
        calc_metrics.previous_period_start,
        calc_metrics.previous_period_end,
        calc_metrics.franchise_name,
        calc_metrics.product,
        calc_metrics.metric_name,
        calc_metrics.value_type,
        calc_metrics.current_value,
        calc_metrics.previous_value
FROM (
    SELECT  main.reporting_date,
            main.reporting_period,
            main.reporting_period_code, 
            main.period_start,
            main.period_end,
            main.previous_period_start,
            main.previous_period_end,
            main.franchise_name,
            main.product,
            calc_budgets.metric_name,
            main.value_type,
            calc_budgets.order_num,
            SAFE_DIVIDE(main.current_value, LEAD(main.current_value) OVER (PARTITION BY main.reporting_date, main.reporting_period_code, main.franchise_name, main.product, main.value_type, calc_budgets.metric_name ORDER BY calc_budgets.order_num)) AS current_value,
            SAFE_DIVIDE(main.previous_value, LEAD(main.previous_value) OVER (PARTITION BY main.reporting_date, main.reporting_period_code, main.franchise_name, main.product, main.value_type, calc_budgets.metric_name ORDER BY calc_budgets.order_num)) AS previous_value
    FROM main 
    INNER JOIN (
        SELECT 'Margin' AS metric_name, 1 AS order_num, 'Gamewin' AS parent_metric_name, '%' AS unit
        UNION ALL
        SELECT 'Margin' AS metric_name, 2 AS order_num, 'Turnover' AS parent_metric_name, '%' AS unit
        UNION ALL
        SELECT 'ARPU' AS metric_name, 1 AS order_num, 'Accounting Revenue' AS parent_metric_name, 'EUR' AS unit
        UNION ALL
        SELECT 'ARPU' AS metric_name, 2 AS order_num, 'Active Customers' AS parent_metric_name, 'EUR' AS unit
    ) AS calc_budgets
    ON main.metric_name = calc_budgets.parent_metric_name 
    WHERE   main.metric_name IN ('Turnover', 'Gamewin', 'Accounting Revenue', 'Active Customers')
) AS calc_metrics
WHERE calc_metrics.order_num = 1