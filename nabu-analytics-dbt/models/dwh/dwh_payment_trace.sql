{{
	config(
		schema='dwh',
		materialized='incremental',
		unique_key = 'payment_trace_key',
		cluster_by = ['date_created']
		)
}}

SELECT  DISTINCT payment_trace_key,
        payment_trace_id, 
        payment_id,
        date_created,
        type,
        status,
        title,
        response
FROM {{ ref('stg_payment_trace') }}
{% if is_incremental() %}
WHERE date_created > (SELECT MAX(date_created) FROM {{ this }})
AND partition_time >= TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)
{% endif %}