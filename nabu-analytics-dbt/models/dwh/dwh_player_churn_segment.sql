{{ config(
    schema='dwh',
    materialized='incremental',
    unique_key = 'segmentation_guid',
    partition_by={
      "field": "evaluation_day",
      "data_type": "date",
      "granularity": "day"
    }
)}}


SELECT {{ dbt_utils.surrogate_key(['player_id', 'franchise_name', 'evaluation_day'])}} as segmentation_guid, 
       player_id,
       franchise_name,
       evaluation_day, 
       inactive_days,
       churn_segment, 
       churn_segment_previous
  FROM {{ ref('stg_churn_segment_history') }} a
{% if is_incremental() %}
 WHERE evaluation_day > (SELECT MAX(evaluation_day) FROM {{this}})
{% endif %}


