{{
    config(
		schema='dwh',
		materialized='table',
        )
}}

SELECT 
    Franchise_Name AS franchise,
    Product AS product,
    Budget_Year AS budget_year,
    Budget_Month AS budget_month,
    DATE(Budget_Year, Budget_Month, 1) AS reporting_month,
    Metric AS metric_name,
    Unit AS unit,
    CAST (Budget_Amount as NUMERIC) as budget_amount
 FROM {{ source('staging', 'stg_throne_budget_values') }}
