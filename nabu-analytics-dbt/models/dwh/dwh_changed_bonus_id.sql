{{
	config(
		schema='dwh',
		materialized='table'
		)
}}
{%- set bonus_table_name = 'dm.f_bonus' if target.name == 'prod' else target.schema + '.f_bonus' -%}
{%- set bet_table_name = 'dm.f_bet' if target.name == 'prod' else target.schema + '.f_bet' -%}
SELECT DISTINCT bonus_id
FROM {{ ref('dwh_bonus_log') }}
WHERE bet_id IN (
	SELECT DISTINCT bet_id
	FROM {{ ref('dwh_bet') }}
	WHERE date_updated > (SELECT MAX(final_status_date) FROM {{ bet_table_name }})
) 
	OR bonus_id IN (
		SELECT DISTINCT bonus_id
		FROM {{ ref('dwh_bonus') }}
		WHERE date_updated > (SELECT MAX(date_updated) FROM {{ bonus_table_name }}) 
	)