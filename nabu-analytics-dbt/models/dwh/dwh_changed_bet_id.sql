{{ 
    config(
        schema='dwh'
        )
}}

-- Get the list of bet_id values where actual change happened during last 3 days
-- The list is used in F_BET incremental loading so that only those bet_id values
-- will be used in MERGE statement.
SELECT
    DISTINCT(bet_id)
FROM {{ ref('dwh_bet') }}
WHERE date_updated > TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL -3 DAY)