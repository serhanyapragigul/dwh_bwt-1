{{ config(schema='dwh', materialized='table')}}
    
with prep as
(
  select player_id, 
         franchise_name,
         sum(value_segment_change) over (partition by player_id, franchise_name ORDER BY evaluation_date) as val_groups,
         value_segment,
         evaluation_date
  from (
     select player_id,
          franchise_name,
          evaluation_date,
          case when value_segment != LAG(value_segment) OVER (PARTITION BY player_id, franchise_name ORDER BY evaluation_date) THEN 1 ELSE 0 END as value_segment_change,
          value_segment
     FROM {{ ref('stg_value_segment_history') }}
  )
)
 SELECT player_id,
        franchise_name,
        min(evaluation_date) as effective_from,
        case when val_groups = max(val_groups) over (partition by player_id, franchise_name order by val_groups rows between unbounded preceding and unbounded following) 
          THEN DATE('9999-12-31')
        ELSE max(evaluation_date)
        END as effective_to,
        max(value_segment) as value_segment
   FROM prep
  group by player_id, franchise_name, val_groups 