{{ config(schema='dwh', materialized='table')}}
    
SELECT player_id,
       franchise_name,
       evaluation_date,
       value_segment,
       value_seg_30d_highest
FROM (
    SELECT
        player_id,
        franchise_name,
        LAST_VALUE(evaluation_date) OVER (PARTITION BY player_id, franchise_name ORDER BY evaluation_date) AS evaluation_date,
        LAST_VALUE(value_segment) OVER (PARTITION BY player_id, franchise_name ORDER BY evaluation_date) AS value_segment,
        LAST_VALUE(value_seg_30d_highest) OVER (PARTITION BY player_id, franchise_name ORDER BY evaluation_date) AS value_seg_30d_highest,
        row_number() OVER (PARTITION BY player_id, franchise_name ORDER BY evaluation_date) AS rn
    FROM {{ ref('stg_value_segment') }}
)
WHERE rn = 1