{{ config(schema='dwh', materialized='table') }}

SELECT 'CLAIMED' AS platform_bonus_status, 'CLAIMED' AS dwh_bonus_status, FALSE AS is_final_bonus_status, TRUE AS is_active_bonus_status
UNION ALL 	
SELECT 'DEADLINE_PASSED' AS platform_bonus_status, 'DEADLINE_PASSED'  AS dwh_bonus_status, TRUE AS is_final_bonus_status ,FALSE AS is_active_bonus_status
UNION ALL 
SELECT 'COMPLETED' AS platform_bonus_status, 'COMPLETED' AS dwh_bonus_status, TRUE AS is_final_bonus_status, FALSE AS is_active_bonus_status
UNION ALL 	
SELECT 'DEPOSIT_BONUS_BLOCKED' AS platform_bonus_status, 'BLOCKED' AS dwh_bonus_status, TRUE as is_final_bonus_status, FALSE AS is_active_bonus_status
UNION ALL 	
SELECT 'DELIVERED' AS platform_bonus_status, 'DELIVERED' AS dwh_bonus_status, FALSE AS is_final_bonus_status, FALSE AS is_active_bonus_status
UNION ALL 	
SELECT 'LOCKUP_BONUS_BLOCKED' AS platform_bonus_status, 'BLOCKED' AS dwh_bonus_status, TRUE AS is_final_bonus_status, FALSE AS is_active_bonus_status
UNION ALL 
SELECT 'REJECTED' AS platform_bonus_status, 'REJECTED' AS dwh_bonus_status, TRUE AS is_final_bonus_status, FALSE AS is_active_bonus_status
UNION ALL 
SELECT 'WAGERED' AS platform_bonus_status, 'WAGERED' AS platform_bonus_status, FALSE AS is_final_bonus_status, TRUE AS is_active_bonus_status
UNION ALL
SELECT 'AMOUNT_BONUS_BLOCKED' AS platform_bonus_status, 'BLOCKED' AS dwh_bonus_status, TRUE AS is_final_bonus_status, FALSE AS is_active_bonus_status