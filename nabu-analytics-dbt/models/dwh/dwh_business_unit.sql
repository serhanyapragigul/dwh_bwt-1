{{ config(schema='dwh')}}

SELECT 
                business_unit_id, 
                business_unit_name,
                business_unit_type,	
                parent_id,	
                host_name,	
                initial_player_status,	
                min_player_age,	
                base_currency,	
                restricted,	
                sharing_strategy,	
                date_created,	
                start_date,	
                SAFE_CAST(COALESCE(LEAD(start_date) OVER (PARTITION BY business_unit_id ORDER BY start_date ASC), '9999-12-31 00:00:00') AS TIMESTAMP) as end_date
 FROM
                (

                        SELECT 
                        DISTINCT

                        business_unit_id, 
                        business_unit_name,
                        business_unit_type,	
                        parent_id,	
                        host_name,	
                        initial_player_status,	
                        min_player_age,	
                        base_currency,	
                        restricted,	
                        sharing_strategy,	
                        min(date_updated) date_created,	
                        min(date_updated) start_date
                        
                        from {{ ref('stg_business_unit') }}

                        group by 
                        business_unit_id, 
                        business_unit_name,
                        business_unit_type,	
                        parent_id,	
                        host_name,	
                        initial_player_status,	
                        min_player_age,	
                        base_currency,	
                        restricted,	
                        sharing_strategy
                )
GROUP BY 
business_unit_id, 
business_unit_name,
business_unit_type,	
parent_id,	
host_name,	
initial_player_status,	
min_player_age,	
base_currency,	
restricted,	
sharing_strategy,	
date_created,	
start_date
