{{
        config(
                schema = 'dwh',
                materialized = 'table',
                partition_by={
                "field": "retrieval_date",
                "data_type": "date",
                "granularity": "day"
                },
                cluster_by = ['retrieval_date', 'is_valid_email', 'is_fraud_status', 'clean_email'],
                tags = ['daily']
        )
}}

SELECT  realm_player_id,
        email,
        status_name,
        retrieval_date,
        realm_segment,
        LOWER(current_sb_classification) AS current_sb_classification,
        CASE
	        WHEN email LIKE '%+%@%' 
		THEN REPLACE(CONCAT(SUBSTRING(email, 0, INSTR(email, '+')-1), SUBSTRING(email, INSTR(email, '@'))), '.', '')
		ELSE REPLACE(email, '.', '')
	END AS clean_email,
        email IS NOT NULL
        	AND REGEXP_CONTAINS(email, r"@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+")
		        AND NOT REGEXP_CONTAINS(email, r"\*\*") AS is_valid_email,  
        -- HasToSendDocuments status was removed from the list of fraud on 2021-08-17 according to Bahadir
        status_name IN ('FakeInfo', 'BonusAbuser', 'Fraud', 'RestrictedCountry', 'Frozen', 'Chargeback', 'SecurityCheck', 'PokerFraud', 'CreditCardFraud', 'FakeDocuments') AS is_fraud_status
FROM    {{ ref('stg_realm_customer_status') }}