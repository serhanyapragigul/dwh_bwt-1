
{{ config(
        schema='dwh',
        materialized='incremental',
        unique_key = 'user_agent')
}}

SELECT
      U.ua AS user_agent
    , U.type AS user_agent_type
    , U.brand AS user_agent_brand
    , U.name AS user_agent_name
    , U.url AS user_agent_url
    , os.name AS user_agent_os_name
    , os.code AS user_agent_os_code
    , os.url AS user_agent_os_url
    , os.family AS user_agent_os_family
    , os.family_code AS user_agent_os_family_code
    , os.family_vendor AS user_agent_os_family_vendor
    , os.icon AS user_agent_os_icon
    , os.icon_large AS user_agent_os_icon_large
    , SAFE_CAST(device.is_mobile_device AS BOOL) AS user_agent_device_is_mobile_device
    , device.type AS user_agent_device_type
    , device.brand AS user_agent_device_brand
    , device.brand_code AS user_agent_device_brand_code
    , device.brand_url AS user_agent_device_brand_url
    , device.name AS user_agent_device_name
    , browser.name AS user_agent_browser_name
    , browser.version AS user_agent_browser_version
    , browser.version_major AS user_agent_browser_version_major
    , browser.engine AS user_agent_browser_engine
    , SAFE_CAST(crawler.is_crawler AS BOOL) AS user_agent_crawler_is_crawler
    , crawler.category AS user_agent_crawler_category
    , crawler.last_seen AS user_agent_crawler_last_seen
    , U.request_user_agent AS request_user_agent
    , U.request_datetime AS request_datetime
FROM {{ source('userstack_in', 'in_user_agents') }} AS U, 
    UNNEST(os) AS os, 
    UNNEST(device) AS device, 
    UNNEST(browser) AS browser,
    UNNEST(crawler) AS crawler
WHERE 1 = 1
AND U.request_user_agent IS NOT NULL
{% if is_incremental() %}

        AND U.request_datetime > (SELECT MAX(request_datetime) FROM {{ this }})

{% endif %}
QUALIFY ROW_NUMBER() OVER(PARTITION BY U.ua ORDER BY U.request_datetime DESC) = 1
