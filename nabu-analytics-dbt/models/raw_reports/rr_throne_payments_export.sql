  {{ config(schema='raw_reports')}}


  	SELECT DISTINCT
		'THRONE' AS operator,
		FORMAT_DATETIME('%Y-%m-%dT%H:%M:%S', DATETIME(pmt.date_updated)) AS status_date,
		FORMAT_DATETIME('%Y-%m-%dT%H:%M:%S', DATETIME(crtd.create_date)) AS create_date,
		brand.brand_name,
		crtd.currency_code,
		CAST(COALESCE(pmt.transaction_amount, crtd.amount) AS NUMERIC) AS amount,
		CAST(COALESCE(pmt.base_amount, crtd.base_amount) AS NUMERIC) AS amount_eur,
		sesh.country AS country_code,
		COALESCE(pmt.payment_type, crtd.payment_type) AS payment_type,
		UPPER(prv.name) AS provider_name,
		UPPER(COALESCE(pmt.method_type, crtd.method_type)) AS method_name,
		COALESCE(pmt.player_id, crtd.player_id) AS customer_guid,
		COALESCE(pmt.external_reference, crtd.external_reference) AS public_payment_id,
		pmt.payment_status AS status,
		sesh.browser,
		sesh.platform AS os,
		pmt.reason,
		CAST(COALESCE(pmt.transaction_fee, crtd.transaction_fee) AS NUMERIC) AS fee,
		CAST(COALESCE(pmt.fee, crtd.fee_eur) AS NUMERIC) AS fee_eur,
		CASE 
			WHEN LENGTH(acc.external_account_id) = 16 AND acc.external_account_id LIKE '%*****%'
			THEN LEFT(acc.external_account_id, 6)
		END AS bin_series,
		UPPER(chnl.payment_channel_name) AS channel_name,
		FALSE AS is_private_lobby,
		COALESCE(pmt.payment_id = ndc.first_deposit_id, FALSE) AS is_first_deposit,
		pmt.payment_id as internal_payment_id,
		pmt.date_updated AS json_ignore_timestamp	
	FROM {{ ref('stg_payment') }} AS pmt
	
	-- join on created date
	LEFT JOIN (
		SELECT
			pmt.payment_id,
			FIRST_VALUE(pmt.date_updated IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS create_date,
			FIRST_VALUE(pmt.transaction_currency IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS currency_code,
			LAST_VALUE(pmt.transaction_amount IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS amount,
			LAST_VALUE(pmt.base_amount IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS base_amount,
			FIRST_VALUE(pmt.payment_account_id IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_account_id,
			FIRST_VALUE(pmt.payment_type IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_type,
			FIRST_VALUE(pmt.provider_id IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS provider_id,
			FIRST_VALUE(pmt.method_type IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS method_type,
			FIRST_VALUE(pmt.player_id IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS player_id,
			LAST_VALUE(pmt.external_reference IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS external_reference,
			LAST_VALUE(pmt.transaction_fee IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS transaction_fee,
			LAST_VALUE(pmt.fee IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS fee_eur,
			LAST_VALUE(pmt.payment_channel_id IGNORE NULLS) OVER (PARTITION BY pmt.payment_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS payment_channel_id
		FROM {{ ref('stg_payment') }} AS pmt
	) AS crtd
		ON pmt.payment_id = crtd.payment_id

	-- join on franchise name
	LEFT JOIN (
		SELECT DISTINCT
			bu.business_unit_id,
			UPPER(LAST_VALUE(bu.business_unit_name) OVER (PARTITION BY bu.business_unit_id ORDER BY bu.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) AS brand_name
		FROM {{ ref('stg_business_unit') }} AS bu
	) AS brand
		ON pmt.business_unit_id = brand.business_unit_id

	-- join on payment account to get BIN series / country
	LEFT JOIN {{ ref('stg_payment_account') }} AS acc 
		ON COALESCE(pmt.payment_account_id, crtd.payment_account_id) = acc.payment_accounts_id 

	-- join on provider name
	LEFT JOIN {{ ref('stg_payment_provider') }} AS prv
		ON COALESCE(pmt.provider_id, crtd.provider_id) = prv.payment_provider_id 

	-- join user session information - only uses sessions created in the past 24 hours and caps a player session expiration at the earliest of session expiration, next session start, or current_timestamp
	LEFT JOIN (
		SELECT
			player_id,
			date_created,
			LEAST(COALESCE(date_expiration, CURRENT_TIMESTAMP()), COALESCE(LEAD(date_created) OVER (PARTITION BY player_id ORDER BY date_created ASC), CURRENT_TIMESTAMP())) AS date_expiration,
			country,
			UPPER(
				CONCAT(browser,
					CASE WHEN UPPER(browser_version) IN ('0', 'UNKNOWN') OR browser_version IS NULL THEN ''
						ELSE CONCAT('.V', CAST(browser_version AS STRING))
					END
				)
			) AS browser,
			UPPER(
				CONCAT(platform,
					CASE WHEN UPPER(platform_version) IN ('0', 'UNKNOWN') OR platform_version IS NULL THEN ''
						ELSE CONCAT('.V', CAST(platform_version AS STRING))
					END
				)
			) AS platform
		FROM {{ ref('stg_session') }}
		WHERE type = 'CREATED'
		AND date_created >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR)
	) AS sesh
		ON COALESCE(pmt.player_id, crtd.player_id) = sesh.player_id
		AND pmt.date_updated >= sesh.date_created
		AND pmt.date_updated < sesh.date_expiration

	-- join payment channel name for withdrawals
	LEFT JOIN {{ ref('stg_payment_channel') }} AS chnl
		ON COALESCE(pmt.payment_channel_id, crtd.payment_channel_id) = chnl.payment_channel_id
		
	-- join on first deposit id
	LEFT JOIN (
		SELECT DISTINCT
			pmt.player_id,
			FIRST_VALUE(pmt.payment_id IGNORE NULLS) OVER (PARTITION BY pmt.player_id ORDER BY pmt.date_updated ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS first_deposit_id
		FROM {{ ref('stg_payment') }} AS pmt
		WHERE pmt.payment_type = 'DEPOSIT'
	) AS ndc
		ON pmt.player_id = ndc.player_id
			
	--WHERE pmt.date_updated >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 HOUR) -- remove when reinitialising, add back iater