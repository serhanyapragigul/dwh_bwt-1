{{ 
        config(
            schema='dq', 
            materialized='table'
            ) 
}}

SELECT 
    ignore_key,
    test_name,
    is_permanent,
    'USER' as updated_by,
    COALESCE(reason, 'added by user') as reason
FROM {{ ref('dq_testignore_manual') }}