{% snapshot dwh_country_codes_snapshot %}

    {{
        config(
          target_schema=generate_schema_name('dwh'),
          strategy='check',
          unique_key='code',
          check_cols=['name'],
        )
    }}

    select UPPER(code) AS code, name from {{ ref('stg_country_codes_seed') }}

{% endsnapshot %}