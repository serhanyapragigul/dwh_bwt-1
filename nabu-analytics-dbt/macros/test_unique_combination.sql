{% test unique_combination(model) %}

{%- set columns = kwargs.get('combination_of_columns', '1') %}
{%- set columns_str = ', '.join(columns) %}

WITH validation_errors AS (
    SELECT {{ columns_str }}, count(*) as duplicate_count
    FROM {{ model }}
    GROUP BY {{ columns_str }}
    HAVING count(*) > 1
)

SELECT {{ columns_str }}, duplicate_count
FROM validation_errors

{% endtest %}